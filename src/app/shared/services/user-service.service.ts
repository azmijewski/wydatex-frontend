import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../models/user';
import {environment} from '../../../environments/environment';
import {Registration} from '../models/registration';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {

  constructor(private http: HttpClient) {
  }

  public getCurrentUser(): Observable<User> {
    return this.http.get<User>(`/api/users/login`);
  }

  public registration(registration: Registration): Observable<any> {
    return this.http.post(`/api/users/registration`, registration);
  }

  public emailVerification(code: string): Observable<any> {
    return this.http.put(`/api/users/confirmation/${code}`, null);
  }

  public resendVerificationCode(email: string): Observable<any> {
    return this.http.put(`/api/users/verification-code`, {'email': email});
  }
}
