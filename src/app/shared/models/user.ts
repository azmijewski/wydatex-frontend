export interface User {
  id: string;
  email: string;
  displayName: DisplayName;
  role: Role;
}

export interface DisplayName {
  firstname: string;
  lastName: string;
}

export enum Role {
  ROLE_USER = 'ROLE_USER',
  ROLE_ADMIN = 'ROLE_ADMIN'
}
