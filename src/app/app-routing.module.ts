import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthRoutingModule} from './auth/auth-routing.module';
import {ComesRoutingModule} from './comes/comes-routing.module';

const routes: Routes = [{
  path: 'auth',
  loadChildren: () => AuthRoutingModule
}, {
  path: '',
  loadChildren: () => ComesRoutingModule
}];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
