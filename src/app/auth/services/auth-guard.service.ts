import { Injectable } from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot} from '@angular/router';
import {AuthenticationService} from './authentication.service';
import {UserServiceService} from '../../shared/services/user-service.service';
import {catchError, map, tap} from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuardService implements CanActivate {

  constructor(private authenticationService: AuthenticationService,
              private userService: UserServiceService,
              private router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    if (this.authenticationService.isLogged()) {
      return true;
    }

    if (sessionStorage.getItem('token')) {
      return this.userService.getCurrentUser()
        .pipe(
          tap(user => this.authenticationService.setLogged(user)),
          map(user => user !== null),
          catchError(err => {
            this.authenticationService.logout();
            throw err;
          })
        );
    }
    this.router.navigateByUrl('auth/login');
    return false;
  }
}
