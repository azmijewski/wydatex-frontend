import {Injectable} from '@angular/core';
import {UserServiceService} from '../../shared/services/user-service.service';
import {User} from '../../shared/models/user';
import {AuthState} from '../model/auth-state';
import {Observable} from 'rxjs';
import {catchError, tap} from 'rxjs/operators';
import {Router} from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  current: User;
  state: AuthState;

  constructor(private userService: UserServiceService, private router: Router) {
  }

  public isLogged(): boolean {
    return this.state === AuthState.LOGGED;
  }

  public login(username: string, password: string): Observable<User> {
    const token = btoa(`${username}:${password}`);
    sessionStorage.setItem('token', token);
    return this.userService.getCurrentUser().pipe(
      tap((user) => this.setLogged(user))
    );
  }

  public logout(): void {
    this.state = AuthState.NOT_LOGGED;
    this.current = null;
    sessionStorage.removeItem('token');
    this.router.navigateByUrl('/auth/login');
  }

  public setLogged(user: User): void {
    this.current = user;
    this.state = AuthState.LOGGED;
  }

  public getCurrent(): User {
    return this.current;
  }
}

