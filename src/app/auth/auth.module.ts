import { NgModule } from '@angular/core';
import {AuthRoutingModule} from './auth-routing.module';
import { LoginComponent } from './components/login/login.component';
import {ReactiveFormsModule} from '@angular/forms';
import {BrowserModule} from '@angular/platform-browser';
import { RegistrationComponent } from './components/registration/registration.component';
import { RegisterSuccessComponent } from './components/register-success/register-success.component';
import { EmailConfirmationComponent } from './components/email-confirmation/email-confirmation.component';
import {SharedModule} from '../shared/shared.module';



@NgModule({
  declarations: [LoginComponent, RegistrationComponent, RegisterSuccessComponent, EmailConfirmationComponent],
  imports: [
    AuthRoutingModule,
    ReactiveFormsModule,
    BrowserModule,
    SharedModule
  ]
})
export class AuthModule { }
