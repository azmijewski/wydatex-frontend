import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {UserServiceService} from '../../../shared/services/user-service.service';
import {Registration} from '../../../shared/models/registration';
import {catchError, switchMap} from 'rxjs/operators';
import {Router} from '@angular/router';
import {HttpErrorResponse} from '@angular/common/http';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  emailAlreadyExist = false;

  constructor(private formBuilder: FormBuilder,
              private userService: UserServiceService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]],
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]]
    });
  }

  onSubmit(): void {
    const registration = new Registration(this.registerForm.value.email, this.registerForm.value.password,
      this.registerForm.value.firstName, this.registerForm.value.lastName,
    );
    this.userService.registration(registration).pipe(
      switchMap(() => this.router.navigateByUrl('/auth/completed')),
      catchError(err => {
          const httpError = err as HttpErrorResponse;
          if (httpError && httpError.status === 409) {
            this.emailAlreadyExist = true;
          }
          throw err;
        }
      )
    ).subscribe();
  }

}
