import {Component, OnInit} from '@angular/core';
import {UserServiceService} from '../../../shared/services/user-service.service';
import {catchError, tap} from 'rxjs/operators';
import {ActivatedRoute} from '@angular/router';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'app-email-confirmation',
  templateUrl: './email-confirmation.component.html',
  styleUrls: ['./email-confirmation.component.css']
})
export class EmailConfirmationComponent implements OnInit {
  isDone = false;
  isConfirmed = false;
  emailSubmitted = false;
  resendCodeForm: FormGroup;


  constructor(private userService: UserServiceService,
              private route: ActivatedRoute,
              private formBuilder: FormBuilder) {
  }

  ngOnInit(): void {
    const token = this.route.snapshot.paramMap.get('code');
    if (token) {
      this.userService.emailVerification(token)
        .pipe(
          tap(() => {
            this.isDone = true;
            this.isConfirmed = true;
          }),
          catchError((err, caught) => {
            this.handleError();
            throw err;
          })).subscribe();
    }
  }

  private handleError(): void {
    this.resendCodeForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });
    this.isDone = true;
    this.isConfirmed = false;
  }

  public onSubmit(): void {
    this.userService.resendVerificationCode(this.resendCodeForm.value.email)
      .pipe(
        tap(() => this.emailSubmitted = true)
      ).subscribe();
  }
}
