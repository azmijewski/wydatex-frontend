import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {LoginComponent} from './components/login/login.component';
import {RegistrationComponent} from './components/registration/registration.component';
import {RegisterSuccessComponent} from './components/register-success/register-success.component';
import {EmailConfirmationComponent} from './components/email-confirmation/email-confirmation.component';

const routes: Routes = [{
  path: 'login',
  component: LoginComponent
}, {
  path: '',
  redirectTo: 'login',
  pathMatch: 'full'
}, {
  path: 'registration',
  component: RegistrationComponent,
}, {
  path: 'completed',
  component: RegisterSuccessComponent
}, {
  path: 'email-confirmation/:code',
  component: EmailConfirmationComponent
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AuthRoutingModule {
}
