import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ContentComponent} from './containers/content/content.component';
import {AuthGuardService} from '../auth/services/auth-guard.service';
import {DashboardComponent} from './containers/dashboard/dashboard.component';
import {IncomesComponent} from './containers/incomes/incomes.component';
import {OutcomesComponent} from './containers/outcomes/outcomes.component';

const routes: Routes = [{
  path: '',
  component: ContentComponent,
  canActivate: [AuthGuardService],
  children: [
    {
      path: '',
      redirectTo: 'dashboard',
      pathMatch: 'full'
    },
    {
      path: 'dashboard',
      component: DashboardComponent
    },
    {
      path: 'incomes',
      component: IncomesComponent
    },
    {
      path: 'outcomes',
      component: OutcomesComponent
    }
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class ComesRoutingModule { }
