import {NgModule} from '@angular/core';
import {ComesRoutingModule} from './comes-routing.module';
import {ContentComponent} from './containers/content/content.component';
import {DashboardComponent} from './containers/dashboard/dashboard.component';
import {BrowserModule} from '@angular/platform-browser';
import {MatSidenavModule} from '@angular/material/sidenav';
import { NavbarComponent } from './components/navbar/navbar.component';
import {MatListModule} from '@angular/material/list';
import { ComeItemComponent } from './components/come-item/come-item.component';
import { ComeListComponent } from './components/come-list/come-list.component';
import { IncomesComponent } from './containers/incomes/incomes.component';
import { ComeDatePickerComponent } from './components/come-date-picker/come-date-picker.component';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import { OutcomesComponent } from './containers/outcomes/outcomes.component';
import { AbstractComeComponent } from './containers/abstract-come/abstract-come.component';


@NgModule({
  declarations: [
    ContentComponent,
    DashboardComponent,
    NavbarComponent,
    ComeItemComponent,
    ComeListComponent,
    IncomesComponent,
    ComeDatePickerComponent,
    OutcomesComponent,
    AbstractComeComponent],
  imports: [
    ComesRoutingModule,
    BrowserModule,
    MatSidenavModule,
    MatListModule,
    MatDatepickerModule,
    FormsModule,
    MatInputModule,
    MatNativeDateModule
  ]
})
export class ComesModule {
}
