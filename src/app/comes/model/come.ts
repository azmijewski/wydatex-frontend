export class Come {
  id: string;
  price: number;
  date: Date;
  description: string;
  comeType: ComeType;

  constructor(price: number, date: Date, description: string) {
    this.price = price;
    this.date = date;
    this.description = description;
  }
}

export enum ComeType {
  INCOME = 'INCOME',
  OUTCOME = 'OUTCOME'
}
