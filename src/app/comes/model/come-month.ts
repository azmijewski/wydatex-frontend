import {Come} from './come';

export interface ComeMonth {
  month: number;
  year: number;
  sum: number;
  comes: Come[];
  current: boolean;
}
