import {Component, OnInit, Output, EventEmitter, Input} from '@angular/core';

@Component({
  selector: 'app-come-date-picker',
  templateUrl: './come-date-picker.component.html',
  styleUrls: ['./come-date-picker.component.css']
})
export class ComeDatePickerComponent implements OnInit {

  constructor() {
  }

  @Output() startDateEventEmitter = new EventEmitter<Date>();
  @Output() endDateEventEmitter = new EventEmitter<Date>();

  @Input() startDate: Date;
  @Input() endDate: Date;

  ngOnInit(): void {
  }

  public onStartDateChanged(): void {
    this.startDateEventEmitter.emit(this.startDate);
  }

  public onEndDateChanged(): void {
    this.endDateEventEmitter.emit(this.endDate);
  }

  public startDateFilter = (d: Date | null): boolean => d <= this.endDate;

  public endDateFilter = (d: Date | null): boolean => d >= this.startDate;

}

