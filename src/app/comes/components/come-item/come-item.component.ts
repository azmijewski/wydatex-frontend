import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Come} from '../../model/come';

@Component({
  selector: 'app-come-item',
  templateUrl: './come-item.component.html',
  styleUrls: ['./come-item.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ComeItemComponent implements OnInit {
  @Input() come: Come;

  constructor() { }

  ngOnInit(): void {
  }

}
