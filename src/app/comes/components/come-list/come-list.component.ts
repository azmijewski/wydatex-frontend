import {Component, Input, OnInit} from '@angular/core';
import {Come} from '../../model/come';

@Component({
  selector: 'app-come-list',
  templateUrl: './come-list.component.html',
  styleUrls: ['./come-list.component.css']
})
export class ComeListComponent implements OnInit {
  @Input() comes: Come[];

  constructor() { }

  ngOnInit(): void {
  }

}
