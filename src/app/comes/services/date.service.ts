export class DateService {
  static getStartMonthDay(): Date {
    const date = new Date();
    date.setDate(1);
    return date;
  }

  static getNextMonthStartDay(): Date {
    const date = new Date();
    date.setMonth(date.getMonth() + 1, 1);
    return date;
  }
}
