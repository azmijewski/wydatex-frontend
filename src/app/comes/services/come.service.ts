import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Come} from '../model/come';
import {Observable} from 'rxjs';
import {ComeMonth} from '../model/come-month';

@Injectable({
  providedIn: 'root'
})
export class ComeService {

  constructor(private http: HttpClient) {
  }

  private static getDateParams(start: Date, end: Date): HttpParams {
    return new HttpParams({
      fromObject: {
        startDate: start.toISOString(),
        endDate: end.toISOString(),
      }
    });
  }

  public addIncome(come: Come): Observable<any> {
    return this.http.post('/api/comes/incomes', come);
  }

  public getIncomesBetweenDates(start: Date, end: Date): Observable<Come[]> {
    const dateParams = ComeService.getDateParams(start, end);
    return this.http.get<Come[]>('/api/comes/incomes', {params: dateParams});
  }

  public addOutcome(come: Come): Observable<any> {
    return this.http.post('/api/comes/outcomes', come);
  }

  public getOutcomesBetweenDates(start: Date, end: Date): Observable<Come[]> {
    const dateParams = ComeService.getDateParams(start, end);
    return this.http.get<Come[]>('/api/comes/outcomes', {params: dateParams});
  }

  public deleteCome(id: string): Observable<any> {
    return this.http.delete(`/api/comes/${id}`);
  }

  public getLastComes(): Observable<Come[]> {
    return this.http.get<Come[]>('/api/comes');
  }

  public updateCome(id: string, come: Come): Observable<any> {
    return this.http.put(`/api/comes/${id}`, come);
  }

  public getComesByMonth(): Observable<ComeMonth[]> {
    return this.http.get<ComeMonth[]>('/api/comes/byMonth');
  }
}
