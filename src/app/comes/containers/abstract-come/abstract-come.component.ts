import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {Come} from '../../model/come';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-abstract-come',
  templateUrl: './abstract-come.component.html',
  styleUrls: ['./abstract-come.component.css']
})
export class AbstractComeComponent implements OnInit {
  @Input() comesLoading: (startDate: Date, endDate: Date) =>  Observable<Come[]>;
  public comes$: Observable<Come[]>;

  public startDate = DateService.getStartMonthDay();
  public endDate = DateService.getNextMonthStartDay();

  constructor() { }

  ngOnInit(): void {
    this.comes$ = this.comesLoading(this.startDate, this.endDate);
  }

  public onStartDayChanged(startDate: Date): void {
    this.startDate = startDate;
    this.comes$ = this.comesLoading(startDate, this.endDate);
  }

  public onEndDayChanged(endDate: Date): void {
    this.endDate = endDate;
    this.comes$ = this.comesLoading(this.startDate, endDate);
  }

}
