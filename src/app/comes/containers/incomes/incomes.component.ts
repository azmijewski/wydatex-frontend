import { Component, OnInit } from '@angular/core';
import {ComeService} from '../../services/come.service';
import {Observable} from 'rxjs';
import {Come} from '../../model/come';
import {ComeDatePickerComponent} from '../../components/come-date-picker/come-date-picker.component';
import {DateService} from '../../services/date.service';

@Component({
  selector: 'app-incomes',
  templateUrl: './incomes.component.html',
  styleUrls: ['./incomes.component.css']
})
export class IncomesComponent {

  constructor(private comeService: ComeService) { }

  public getIncomes = (startDate: Date, endDate: Date) =>
    this.comeService.getIncomesBetweenDates(startDate, endDate)

}
