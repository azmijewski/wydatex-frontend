import { Component } from '@angular/core';
import {ComeService} from '../../services/come.service';

@Component({
  selector: 'app-outcomes',
  templateUrl: './outcomes.component.html',
  styleUrls: ['./outcomes.component.css']
})
export class OutcomesComponent {
  constructor(private comeService: ComeService) { }

  public getOutcomes = (startDate: Date, endDate: Date) =>
    this.comeService.getOutcomesBetweenDates(startDate, endDate)

}
