import {Component, OnInit, ViewChild} from '@angular/core';
import {MatSidenav} from '@angular/material/sidenav';
import {ScreenSizeService} from '../../../shared/services/screen-size.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  @ViewChild ('drawer', { static: true }) public drawer: MatSidenav;

  constructor(public screnSizeService: ScreenSizeService) { }

  ngOnInit(): void {
  }

}
